package com.orsyp.runtimedashboard.model;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Vector;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.servlet.ServletContext;

public class Uvms implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public String server;
	public int port;
	public String user;
	public String password;
	public String area;
	public String company;	
	public Vector<String> nodeFilter = new Vector<String>();
	
	public int twStartMinute = 15;
	public int twLength = 30;
	
	
	public static Uvms loadUvmsFromFile(ServletContext servletContext) {
		String filename = servletContext.getRealPath("")+File.separator+"WEB-INF/config/uvms";
		if (!(new File(filename).exists())) {
			System.out.println("Config file not found: "+filename);
			return null;
		}
		System.out.println("Loading config file '"+filename+"' ...");
		try {
			FileInputStream fos = new FileInputStream(filename);
			GZIPInputStream gz = new GZIPInputStream(fos);
			BufferedInputStream buf = new BufferedInputStream(gz);
			
			ObjectInputStream oos = new ObjectInputStream(buf);			
			Uvms uvms = (Uvms) oos.readObject();
			if (uvms.nodeFilter==null)
				uvms.nodeFilter=new Vector<String>();
			return uvms; 
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return null;
	}
	
	public void saveConfiguration(ServletContext servletContext) throws Exception {
		String filename = servletContext.getRealPath("")+File.separator+"WEB-INF/config/uvms";		
		FileOutputStream fo = new FileOutputStream( new File(filename));
		GZIPOutputStream gz = new GZIPOutputStream(fo);					   
		BufferedOutputStream bf = new BufferedOutputStream(gz);
		ObjectOutputStream oout = new ObjectOutputStream(bf);			
		oout.writeObject(this);
		oout.close();
	}
	
}
