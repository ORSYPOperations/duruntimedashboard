package com.orsyp.runtimedashboard.model;

import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;

public class SimulationResult {
	
	public String task;
	public String session;
	public String uproc;
	public String mu;
	public String node;
	public String host;
	public LocalTime estimatedStartTime;
	private long duration;	
	private String durationStr;
	private String finishTimeStr;
	
	
	public long getDuration() {
		return duration;
	}

	public String getDurationStr() {
		return durationStr;
	}
	
	public void setAvgDuration(long duration) {
		this.duration = duration;

		durationStr = "&lt;1 s";
		
		if (duration>0) {
			int mins = (int) duration / 60000;
			if (mins>0)
				durationStr = String.valueOf(mins) + " m";
			else	
			if (mins==0) {				
				int secs =	(int) duration / 1000;
				if (secs>0)
					durationStr = String.valueOf(secs) + " s";
			}
		}
		
		if (estimatedStartTime==null)
			return;
		
		String durStr = "";
		
		if (duration>0) {
			finishTimeStr= estimatedStartTime.plusMillis((int)duration).toString(DateTimeFormat.forPattern("HH:mm"));
			//check if it ends in the next day or later
			int days =0;
			LocalTime finishTime = estimatedStartTime.plusMillis((int)duration);
			if (finishTime.getHourOfDay()<estimatedStartTime.getHourOfDay()
				|| (finishTime.getHourOfDay()==estimatedStartTime.getHourOfDay() && finishTime.getMinuteOfHour()<estimatedStartTime.getMinuteOfHour()))
				days=1;
			//longer than a day
			if (duration>24*60*60*1000)
				days+= duration/(24*60*60*1000);
			if (days>0)
				finishTimeStr+=" (+"+days+")";
			
			if (duration>24*60*60*1000)
				durStr = " - "+(duration/3600000)+"h";
			else {
				durStr = " -";
				LocalTime lt = LocalTime.fromMillisOfDay(duration);
				if (lt.getHourOfDay()>0)
					durStr+= " " + lt.getHourOfDay() + "h";
				if (lt.getMinuteOfHour()>0)
					durStr+= " " + lt.getMinuteOfHour() + "m";
				
				if (duration<1000)
					durStr+= " <1s";
				else
					if (lt.getHourOfDay()==0 && lt.getMinuteOfHour()<10)
						durStr+= " " + lt.getSecondOfMinute() + "s";
			}
			if (duration<1000)
				durStr = " - <1sec";
			else
			if (duration<60000)
				durStr = " - "+(duration/1000)+"s";				
			if (duration<3600000) {
				durStr = " - "+(duration/60000)+"m";
				if (duration/60000 < 10)
					durStr += " " + ((duration%60)/1000)+"s";
			}
			else
				durStr = " - "+(duration/3600000)+"h " +((duration%3600000)/60000)+"m";			
		}
		else
			finishTimeStr= estimatedStartTime.toString(DateTimeFormat.forPattern("HH:mm"));
		
		finishTimeStr += durStr;
				
	}

	public String getFinishTime() {
		return finishTimeStr;
	}

		
}
