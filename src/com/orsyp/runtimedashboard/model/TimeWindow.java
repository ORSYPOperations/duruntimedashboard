package com.orsyp.runtimedashboard.model;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;

import com.orsyp.runtimedashboard.AppManager;

public class TimeWindow {

	private LocalTime start;
	private LocalTime end;
	
	public TimeWindow() {
		setup(LocalTime.now());
	}
	
	public TimeWindow(LocalTime t) {
		setup(t);		
	}
	
	private void setup(LocalTime t) {
		t = t.withSecondOfMinute(0);
		start = t.withMinuteOfHour(0);
		end = start.plusHours(1);
		
		if (AppManager.isConfigured()) {
			Uvms u = AppManager.getUvms();
			
			if (u.twLength<5)
				u.twLength=30;
			int min = t.getMinuteOfHour();
			
			//get the first start of an interval in an hour
			int firstStart = u.twStartMinute;
			if (firstStart> u.twLength)
				firstStart = firstStart % u.twLength;
			
			//set tw start
			if (min<firstStart) {
				if (t.getHourOfDay()==0)
					start = start.withMinuteOfHour(0);
				else {
					start = start.withMinuteOfHour(firstStart);
					start = start.minusMinutes(u.twLength);
				}
			}
			
			if (min<u.twStartMinute)
				min+=60;
			for (int startMin = u.twStartMinute; startMin<= u.twStartMinute+60; startMin+= u.twLength)
				if (min>=startMin && min<startMin+u.twLength) { 
					start = start.withMinuteOfHour(startMin % 60);
					break;
				}
			
			//set tw end
			end = start.plusMinutes(u.twLength);
			if (end.getHourOfDay()<start.getHourOfDay()) 
				end = LocalTime.parse("00:00");
			
		}
		/*
		if (start.getMinuteOfHour() >= 30) {
			start = start.withMinuteOfHour(30);
			end = end.withMinuteOfHour(0);
			end = end.plusHours(1);			
		}
		else {
			start = start.withMinuteOfHour(0);
			end = end.withMinuteOfHour(30);
		}
		*/
		
	}
	
	public LocalTime getStart() {
		return start;
	}
	
	public LocalTime getEnd() {
		return end;
	}

	public String asString() {				
		return start.toString(DateTimeFormat.forPattern("HH:mm")) + " - " + end.toString(DateTimeFormat.forPattern("HH:mm"));
	}

	public boolean includes(DateTime dt) {
		LocalTime t = new LocalTime(dt.getMillis());
		return includes(t);
	}
	
	public boolean includes(LocalTime t) {
		if (t==null)
			return false;
		return !t.isBefore(start) && !t.isAfter(end);
	}

}
