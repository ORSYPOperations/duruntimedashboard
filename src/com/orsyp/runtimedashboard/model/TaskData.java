package com.orsyp.runtimedashboard.model;

import java.util.HashMap;

import org.joda.time.LocalTime;

import com.orsyp.api.task.TaskId;

public class TaskData {
	
	public TaskId id;	
	
	//task duration from session stats
	private long averageDurationMSecs;

	//estimated start time for each day of the week
	private HashMap<Integer,LocalTime> dailyStartTimes = new HashMap<Integer,LocalTime>();
	private boolean hasStartEstimation = false;
	private boolean hasDurationStats = false;

	public TaskData(TaskId id) {
		this.id = id;
	}
	
	public void addStartTime(int day, LocalTime time) {
		hasStartEstimation=true;
		dailyStartTimes.put(Integer.valueOf(day), time);
	}
	
	public LocalTime getStartTime(int day) {
		return dailyStartTimes.get(day);
	}

	public boolean hasFullEstimationData() {
		return hasDurationStats && hasStartEstimation;
	}
	
	public boolean hasStartEstimation() {
		return hasStartEstimation;
	}
	
	public boolean hasDurationStats() {
		return hasDurationStats;
	}
	
	public long getAverageDurationMSecs() {
		return averageDurationMSecs;
	}

	public void setAverageDurationMSecs(long averageDurationMSecs) {
		hasDurationStats=true;
		this.averageDurationMSecs = averageDurationMSecs;
	}

}
