package com.orsyp.runtimedashboard.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;

import com.orsyp.api.execution.ExecutionItem;
import com.orsyp.api.hr.HrStatisticId;
import com.orsyp.api.hr.HrStatisticItem;
import com.orsyp.api.task.TaskId;
import com.orsyp.runtimedashboard.utils.TimeUtils;

public class NodeData {
	
	//session name - data object
	private HashMap<String,SessionData> sessions = new HashMap<String,SessionData>();
	//uproc name - data object
	private HashMap<String,UprocData> uprocs = new HashMap<String,UprocData>();
	//task id string - data object
	private HashMap<String,TaskData> tasks = new HashMap<String,TaskData>();
	
	private List<ExecutionItem> executions = new ArrayList<ExecutionItem>();
	private List<HrStatisticItem> stats = new ArrayList<HrStatisticItem>();
	
	private List<SimulationResult> currentSimulation = new ArrayList<SimulationResult>();
	private List<SimulationResult> nextSimulation = new ArrayList<SimulationResult>();
	private LocalDate currentSimulationDate;
	
	//each uproc (executed in a task/mu) has estimations for each day of the week and a duration taken from stats
	//task/mu/uproc - data object
	private HashMap<String,UprocEstimation> uprocEstimations = new HashMap<String,UprocEstimation>();
	
	private static final int H24 = 1000 * 60 *60*24;
	private static final int H12 = H24/2;
	
	public void clear() {
		uprocs.clear();
		sessions.clear();
		tasks.clear();
		uprocEstimations.clear();
	}
	
	public void addSession(String name, String label) {
		sessions.put(name,new SessionData(label));
	}
	
	public void addUproc(String name, String label) {
		uprocs.put(name,new UprocData(label));
	}
	
	public void addSessionUprocs(String name, String[] uprocs) {
		sessions.get(name).uprocs = uprocs;
	}
	
	public void addTask(String name, TaskId id) {
		tasks.put(name,new TaskData(id));
	}
	

	public String getUprocLabel(String uproc) {
		UprocData ud = uprocs.get(uproc);
		if (ud==null || ud.label==null)
			return "";
		
		return ud.label;
	}

	public String getSessionLabel(String sess) {		
		SessionData sd = sessions.get(sess);
		if (sd==null || sd.label==null)
			return "";
		
		return sd.label;
	}
	
	public List<String> getSessionUprocs(String sess) {
		List<String> l = new ArrayList<String>();
		SessionData sd = sessions.get(sess);
		if (sd!=null) 
			if (sd.uprocs!=null) 
				l.addAll(Arrays.asList(sd.uprocs));
		return l;
	}
	
	public List<String> getTaskNames() {
		List<String> l = new ArrayList<String>();		
		l.addAll(tasks.keySet());
		return l;
	}
	
	public TaskId getTaskId(String name) {
		TaskData td = tasks.get(name);
		if (td!=null)
			return td.id;
		return null;
	}

	public List<TaskId> getTaskIds() {
		List<TaskId> ids = new ArrayList<TaskId>();
		for (TaskData td: tasks.values())
			if (td.id!=null)
				ids.add(td.id);
		return ids;
	}

	public void addExecution(ExecutionItem executionItem) {
		executions.add(executionItem);		
	}
	
	public void addStats(List<HrStatisticItem> stats) {
		this.stats = stats;		
	}
	
	//calc uproc duration and average start times for each day of the week 
	public void calcStartTimes() {
		//key: getKey, value: list of execution date/times
		HashMap<String,List<LocalDateTime>> taskMuUprTimes = new HashMap<String,List<LocalDateTime>>();
		for (ExecutionItem ex : executions) {
			String key = getExecKey(ex.getTaskName(),ex.getMuName(),ex.getUprocName());
			List<LocalDateTime> times = taskMuUprTimes.get(key);
			if (times==null) 
				times = new ArrayList<LocalDateTime>();
			LocalDateTime dt = new LocalDateTime(ex.getBeginDate()).minusHours(TimeUtils.UTCOffsetHours);
			times.add(dt);
			System.out.println("Execution: " + key + " - " + dt.toString(DateTimeFormat.forPattern("HH:mm")));
			taskMuUprTimes.put(key, times);
		}

		//calc average start time based on previous launch times
		for (String key: taskMuUprTimes.keySet()) {
			List<LocalDateTime> datetimes = taskMuUprTimes.get(key);
			
			//split for each day
			HashMap<Integer,List<LocalTime>> timesByDay = new HashMap<Integer, List<LocalTime>>(); 
			for (LocalDateTime t : datetimes) {
				Integer day = t.getDayOfWeek();
				if (!timesByDay.containsKey(day)) 
					timesByDay.put(day, new ArrayList<LocalTime>());
				List<LocalTime> l = timesByDay.get(day);
				l.add(new LocalTime(t));
			}
			
			//create uproc estimation object
			UprocEstimation est = new UprocEstimation();
			
			String headerStr = "Average start time calculation: " + key;
			String lines = "";
			//add average start time for each day of the week
			for (int day=1;day<=7;day++) {				
				LocalTime avgtime = calcAverage(timesByDay.get(day));
				if (avgtime!=null) {
					est.startTimes.put(Integer.valueOf(day), avgtime);					
					lines += "\n day " + day + " - Time: " + avgtime.toString(DateTimeFormat.forPattern("HH:mm")); 
				}
			}
			if (lines.length()>0)
				System.out.println(headerStr + lines);
			
			uprocEstimations.put(key, est);
		}
		
		//get uproc duration from stats 
		for (HrStatisticItem st : stats) 
			if (st.getUprocStatistic()!=null) {
				//build key from stat id
				String key = getExecKey(st.getIdentifier().getTaskName(),st.getIdentifier().getMuName(),st.getIdentifier().getUprocName());
				UprocEstimation est = uprocEstimations.get(key);
				if (est==null)
					System.out.println("Uproc stats: Task/mu/upr not found: " + key);
				else
					try {
						long avg = st.getUprocStatistic().getExecCompleted().getAverageDuration();						
						est.durationMSecs = avg;
						System.out.println("Uproc stats: Task/mu/upr " + key + " - Duration: " + avg );
					} catch (Exception e) {
						System.out.println("Uproc stats: Task/mu/upr " + key + " - ERROR setting duration");
					}
			}
		
		//set task start times
		for (TaskData td : tasks.values()) {
			//set key using header uproc
			String key = getExecKey(td.id.getName(),td.id.getMuName(),td.id.getUprocName());
			UprocEstimation est = uprocEstimations.get(key);
			if (est==null)
				System.out.println("Task start time estimation: no data for task/mu/upr: " + key);
			else
				//copy average daily start times
				for (Entry<Integer,LocalTime> entry: est.startTimes.entrySet()) {
					System.out.println("Task start time estimation: task/mu/upr: " + key + " - Day: " + entry.getKey() + " - Time: " + entry.getValue().toString(DateTimeFormat.forPattern("HH:mm")));
					td.addStartTime(entry.getKey(), entry.getValue());
				}
		}
	}
	
	private LocalTime calcAverage(List<LocalTime> times) {
		if (times==null)
			return null;
		long avg = 0;  	//average
		int counter=0;	//conuter for weighted average
		
		//incrementally adjust the average time for each launch
		for (LocalTime t : times) {				
			long launchTime = t.getMillisOfDay();
			//first time, set average to launch time
			if (counter==0)
				avg = (int)launchTime;				
			else					
			if (launchTime!=avg) {										
				//calc distance between average and launch time using absolute value
				long dist = Math.abs(launchTime-avg);
				//if the distance is more then 12 hours, get the distance on the other side of the day
				if (dist>H12)						
					dist=H24-dist;
				
				//find the distance's sign
				//if launch is before the current average
				if (avg>launchTime) {
					//if launch to average is more than 12 hours,
					//consider the launch to be before the current average time
					if (avg-launchTime < H12)
						dist=-dist;
				}
				//if launch is after the current average
				else {
					//if average to launch is more than 12 hours,
					//consider the launch to be before the current average time
					if (launchTime-avg > H12)
						dist=-dist;						
				}
				
				//update average with new distance
				//(avg*counter)  -> is the current average * its weight
				//avg + dist	 -> is the launch relative to the current average, dist can be negative
				avg = Math.round( ((avg*counter) + avg + dist) / (counter+1) );
				
				//adjust average if it falls outside the day boundaries
				if (avg<0)
					avg+=H24;
				if (avg>H24)
					avg-=H24;
			}
			//System.out.print("+" + counter+"- " + t.toString(DateTimeFormat.shortTime()));
			//System.out.println(" - avg: " + new LocalTime(avg).toString(DateTimeFormat.shortTime()));
			counter++;
		}
					
		return new LocalTime().withMillisOfDay((int)avg);		
	}
	
	//set task durations from session statistics
	public void calcTaskDurations() {		
		for (HrStatisticItem st : stats)
			//check session stats only
			if (st.getSessionStatistic()!=null) {
				HrStatisticId id = st.getIdentifier();
				String key = id.getSessionName() +"-"+ id.getSessionVersion()+"-"+id.getMuName();
				TaskData td = findTask(id.getSessionName(),id.getMuName());
				if (td!=null) {
					long dur = st.getSessionStatistic().getExecCompleted().getAverageDuration();					
					if (dur<0) 
						System.out.println("Session stats: BAD SESSION STATS: " + dur + " - sess/mu:" + key);
					else 
					if (!td.hasDurationStats() || dur!=0) { 
						td.setAverageDurationMSecs(dur);
						System.out.println("Session stats: " + dur + " - sess/mu: " + key);
					}
				}
				else
					if (st.getIdentifier().getTaskName()!=null && st.getIdentifier().getTaskName().length()>0)
						System.out.println("Session stats: sess/mu not found: " + key);
			}		
	}

	private TaskData findTask(String sessionName, String muName) {
		for (TaskData t: tasks.values()) 
			if (t.id.getSessionName().equals(sessionName))
				if (t.id.getMuName().equals(muName))
					return t;
		return null;
	}

	private String getExecKey(String task, String mu, String upr) {
		String t = (task != null) ? task.trim() : "";  
		String m = (mu != null) ? mu.trim() : "";
		String u = (upr != null) ? upr.trim() : "";
		return t + "---" + m + "---" + u;
	}

	public UprocEstimation getUprocEstimation(ExecutionItem item) {
		return uprocEstimations.get(getExecKey(item.getTaskName(), item.getMuName(), item.getUprocName()));
	}

	
	//getters for test reports	
	public HashMap<String, SessionData> getSessions() {
		return sessions;
	}

	public HashMap<String, UprocData> getUprocs() {
		return uprocs;
	}

	public HashMap<String, TaskData> getTasks() {
		return tasks;
	}

	public List<ExecutionItem> getExecutions() {
		return executions;
	}

	public List<HrStatisticItem> getStats() {
		return stats;
	}

	public HashMap<String, UprocEstimation> getUprocEstimations() {
		return uprocEstimations;
	}

	public void setCurrentSimulation(List<SimulationResult> list) {
		currentSimulation = list;		
		currentSimulationDate = new LocalDate();
	}
	
	public void setNextSimulation(List<SimulationResult> list) {
		nextSimulation = list;
	}

	public List<SimulationResult> getPrecalcSimulations(TimeWindow tw) {
		ArrayList<SimulationResult> l = new ArrayList<SimulationResult>();
		//if we are in a new day, switch to the precalculated forecasts for the next day
		LocalDate today = new LocalDate();
		if (currentSimulationDate==null) 
			currentSimulationDate = today;
		else
		if (today.isAfter(currentSimulationDate))
			switchDay();
		
		if (currentSimulation==null) {
			System.out.println("ERROR: No simulation available");
			return l;
		}
			
		for (SimulationResult s : currentSimulation) 
			if (tw.includes(s.estimatedStartTime))
				l.add(s);
		return l;
	}
	
	public List<SimulationResult> getAllSimulations() {
		return currentSimulation;
	}

	public void switchDay() {
		currentSimulation = nextSimulation;
		currentSimulationDate = new LocalDate();
		nextSimulation=null;
	}

	public TaskData getTask(TaskId id) {
		return tasks.get(id.toString());
	}

}
