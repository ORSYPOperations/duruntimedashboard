package com.orsyp.runtimedashboard.model;

public class LaunchCounters {

	public int intimeTasks=0;
	public int lateTasks=0;
	
	public int missingTasks=0;
	public int unexpectedTasks=0;
	public int expectedTasks=0;
	
	public int abortedUprocs=0;
	public int runningUprocs=0;
	
}
