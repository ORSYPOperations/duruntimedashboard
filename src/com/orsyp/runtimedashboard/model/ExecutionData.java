package com.orsyp.runtimedashboard.model;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.joda.time.Minutes;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;

import com.orsyp.api.execution.ExecutionItem;
import com.orsyp.api.execution.ExecutionStatus;
import com.orsyp.runtimedashboard.utils.TimeUtils;

public class ExecutionData {

	public String host;
	public ExecutionItem item;	
	public long durationMSecs = 0;
	public LocalTime estimatedStartTime;
	public long estimatedDurationMSecs = 0;
	public String durationStr = "";
	public String startStr = "";
	public DateTime startTime;
	public DateTime endTime;
	
	public ExecutionData(ExecutionItem item, LocalTime estimatedStartTime, long estimatedDurationMSecs, String hostName) {
		super();
		this.item = item;
		this.estimatedStartTime = estimatedStartTime;
		this.host = hostName;
		this.estimatedDurationMSecs = estimatedDurationMSecs;
		
		startTime = new DateTime(item.getBeginDate().getTime()).minusHours(TimeUtils.UTCOffsetHours);
		endTime = DateTime.now();			
		startStr = startTime.toString(DateTimeFormat.forPattern("HH:mm"));			
		
		if (item.getStatus()!=ExecutionStatus.Running && item.getEndDate()!=null)
			endTime = new DateTime(item.getEndDate().getTime()).minusHours(TimeUtils.UTCOffsetHours);
		
		/*
		String end = endTime.toString(DateTimeFormat.forPattern("HH:mm"));
		System.out.println(start +"->"+ end);
		*/
		
		durationMSecs = Minutes.minutesBetween(startTime, endTime).getMinutes();			
		durationStr = String.valueOf(durationMSecs) + " m";
		if (durationMSecs==0) {				
			int secs =	Seconds.secondsBetween(startTime, endTime).getSeconds();
			durationStr = String.valueOf(secs) + " s";
			if (secs==0)
				durationStr = "&lt;1 s";
		}
	}

	public boolean isLate() {
		if (estimatedStartTime!=null)
			return durationMSecs > estimatedDurationMSecs;
		return false;
	}

	public boolean isUnexpected() { 
		return estimatedStartTime==null;
	}
	
	
	
}
