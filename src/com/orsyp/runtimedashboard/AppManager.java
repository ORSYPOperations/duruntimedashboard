package com.orsyp.runtimedashboard;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContext;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.Minutes;

import com.orsyp.api.execution.ExecutionItem;
import com.orsyp.api.execution.ExecutionStatus;
import com.orsyp.runtimedashboard.api.NodeConnection;
import com.orsyp.runtimedashboard.api.NodeConnection.NodeConnStatus;
import com.orsyp.runtimedashboard.api.UvmsConnection;
import com.orsyp.runtimedashboard.model.ExecutionData;
import com.orsyp.runtimedashboard.model.LaunchCounters;
import com.orsyp.runtimedashboard.model.SimulationResult;
import com.orsyp.runtimedashboard.model.TimeWindow;
import com.orsyp.runtimedashboard.model.UprocEstimation;
import com.orsyp.runtimedashboard.model.Uvms;
import com.orsyp.runtimedashboard.tasks.ForecastTask;
import com.orsyp.runtimedashboard.tasks.NodeForecastReaderThread;
import com.orsyp.runtimedashboard.tasks.NodeInitThread;
import com.orsyp.runtimedashboard.tasks.NodeRuntimeReaderThread;

public class AppManager {
	
	public enum AppStatus {INITIALIZING, READY};
	
	private static Uvms uvms = null;
	private static boolean uvmsIsFuntional = false;
	private static AppStatus status = AppStatus.INITIALIZING;
	private static TreeMap<String,NodeConnection> nodes = null;
	private static ServletContext sctx;
	private static LaunchCounters currentCounters = new LaunchCounters();

	//init application data at startup and set app state
	//called by servlet listener startup task
	public static void init(ServletContext servletContext) {
		try {
			System.out.println("Initializing webapp...");
			sctx=servletContext;
			System.out.println("Loading uvms configuration...");
			getUvms();
			System.out.println("Loading node objects...");
			loadNodesData();
			System.out.println("Calculating forecasts...");
			calcDayForecasts(new LocalDate());
			
			//schedule background pre reading of next day forecasts
			DateTime now = new DateTime();        
			DateTime nextRun = now.withHourOfDay(23).withMinuteOfHour(20);
		    int minutes = Minutes.minutesBetween(now, nextRun).getMinutes();
		    ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
		    scheduler.schedule(new ForecastTask(sctx), minutes, TimeUnit.MINUTES);
		    System.out.println("Next forecast scheduled " + nextRun.toString());
		    
			status = AppStatus.READY;
			System.out.println("Initialization done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//initial setup of all nodes
	private static void loadNodesData() {		
		if (isConfigured()) {
			connectNodes();
			ArrayList<Thread> threads = new ArrayList<Thread>();
			for (NodeConnection nc : getNodes()) {
				Thread t = new NodeInitThread(nc);
				threads.add(t);
				t.start();				
			}

			//wait for all threads
			for (Thread t: threads)
				try {
					t.join();
				} catch (InterruptedException e) {}
		}
		
	}
		
	//calculate forecasts for the requested day
	public static void calcDayForecasts(LocalDate date) {
		//forecast all nodes
		ArrayList<NodeForecastReaderThread> threads = new ArrayList<NodeForecastReaderThread>();
		for (NodeConnection nc : getNodes())
			if (nc.getStatus()==NodeConnStatus.FORECASTING) {
				NodeForecastReaderThread t = new NodeForecastReaderThread(nc,date);
				threads.add(t);
				t.start();
			}
				
		// wait for all threads
		for (NodeForecastReaderThread t: threads)
			try {
				t.join();		
			} catch (InterruptedException e) {}		
	}

	public static AppStatus getAppStatus() {
		return status;
	}

	public static boolean isConnectedToUvms() {
		return uvmsIsFuntional;
	}

	//init node connections
	private static void connectNodes() {
		UvmsConnection conn = new UvmsConnection(uvms.server, uvms.port, uvms.user, uvms.password);
		nodes = new TreeMap<String,NodeConnection>();
		TreeMap<String,String> availableNodes = null;
		try {
			availableNodes = conn.getNodeList(uvms.company, uvms.area);
			uvmsIsFuntional = true;
		} catch (Exception e) {
			e.printStackTrace();
		}		
		if (availableNodes!=null)
			for (Entry<String,String> node : availableNodes.entrySet())
				if (uvms.nodeFilter==null || uvms.nodeFilter.size()==0 || uvms.nodeFilter.contains(node.getKey())) 
					nodes.put(node.getKey(), new NodeConnection(conn, uvms.company, uvms.area, node.getKey(), node.getValue()));
	}
	
	public static boolean isConfigured() {
		return uvms!=null;
	}

	//get uvms connection object, load from file the first time
	public static Uvms getUvms() {		
		if (uvms==null)
			uvms=Uvms.loadUvmsFromFile(sctx);
		return uvms;
	}

	//save uvms configuration to file and reinitialize application data
	public static void saveConfiguration(Uvms u) throws Exception {
		u.saveConfiguration(sctx);
		uvms=u;
		loadNodesData();
	}
	
	//get the list of available noeds
	public static List<NodeConnection> getNodes() {
		ArrayList<NodeConnection> list = new ArrayList<NodeConnection>();
		if (!isConfigured())
			return list;
		
		if (nodes==null)
			connectNodes();
		
		list.addAll(nodes.values());
		return list;
	}
	
	//get number of nodes with the requested status
	public static int getNodeCount(NodeConnStatus status) {
		int count=0;
		for(NodeConnection nc : getNodes())
			if (nc.getStatus()==status)
				count++;
		return count;
	}
	
	//current running uprocs - bottom right panel 
	public static List<ExecutionData> getRunningData(boolean showAllRunning) {
		return getRunningData(new TimeWindow(), showAllRunning);
	}
	
	//current running uprocs - bottom right panel
	public static List<ExecutionData> getRunningData(TimeWindow tw, boolean showAllRunning) {
		int dayOfWeek=LocalDate.now().getDayOfWeek();
		ArrayList<ExecutionData> list = new ArrayList<ExecutionData>();
		
		ArrayList<NodeRuntimeReaderThread> threads = new ArrayList<NodeRuntimeReaderThread>();
		for (NodeConnection nc : getNodes()) 
			if (nc.isReady()){
				NodeRuntimeReaderThread t = new NodeRuntimeReaderThread(nc,tw, showAllRunning);
				threads.add(t);
				t.start();				
			}

		
		List<SimulationResult> expectedTasks = getExpectedData();
		//update counters
		currentCounters = new LaunchCounters();
		currentCounters.expectedTasks = expectedTasks.size();

		// wait for all threads
		for (NodeRuntimeReaderThread t: threads)
			try {
				t.join();				
				
				//count running uprocs outside current time frame
				if (showAllRunning) {
					List<ExecutionItem> nlist =  t.getRunningList();
					for (ExecutionItem ex : nlist)  {
						UprocEstimation est = t.getNodeData().getUprocEstimation(ex);
						ExecutionData ed = new ExecutionData(ex, est.startTimes.get(dayOfWeek), est.durationMSecs, t.getNode().getHostName());
						
						//exclude the ones in the time window, they are included later
						if (!tw.includes(ed.startTime)) {							
							currentCounters.runningUprocs++;
							list.add(ed);
						}
					}
				}
				
				//count running and aborted uprocs in current time frame
				List<ExecutionItem> nlist =  t.getList();
				for (ExecutionItem ex : nlist) {					
					UprocEstimation est = t.getNodeData().getUprocEstimation(ex);
					ExecutionData ed = new ExecutionData(ex, est.startTimes.get(dayOfWeek), est.durationMSecs, t.getNode().getHostName());
										
					if (ex.getStatus()==ExecutionStatus.Running) {
						currentCounters.runningUprocs++;
						list.add(ed);
					}
					else 
					if (ex.getStatus()==ExecutionStatus.Aborted) {
						currentCounters.abortedUprocs++;
						list.add(ed);
					}

				}
				
				//check running tasks
				for (SimulationResult sr : expectedTasks) {
					boolean running = false;
					boolean executed = false;
					boolean late = false;
					
					for (ExecutionData ed : list)
						if (ed.item!=null && sr!=null)
							//check only executions in current node
							if (ed.item.getNodeName().equals(t.getNode().getNodeName()))
								try {
									if ( ((ed.item.getTaskName()==null && sr.task==null) || ed.item.getTaskName().equals(sr.task)) &&
										 ((ed.item.getSessionName()==null && sr.session==null) || ed.item.getSessionName().equals(sr.session)) &&
										 ((ed.item.getMuName()==null && sr.mu==null) || ed.item.getMuName().equals(sr.mu))) {
										executed=true;
										if (ed.item.getStatus() == ExecutionStatus.Running) 
											running=true;
										if (ed.isLate())
											late=true;
									}
								} catch (Exception e) {}
					
					//check if late or in time
					if (executed) { 
						if (late)
							currentCounters.lateTasks++;
						else
							currentCounters.intimeTasks++;
					}
				}
				
				List<String> unexpTasks = new ArrayList<String>();
				//check unexpected tasks
				for (ExecutionData ed : list) {
					boolean found = false;
					for (SimulationResult sr : expectedTasks) 
						if (ed.item!=null && sr!=null)
							try {
								if ( ((ed.item.getTaskName()==null && sr.task==null) || ed.item.getTaskName().equals(sr.task)) &&
									 ((ed.item.getSessionName()==null && sr.session==null) || ed.item.getSessionName().equals(sr.session)) &&
									 ((ed.item.getMuName()==null && sr.mu==null) || ed.item.getMuName().equals(sr.mu))) {
									found = true;
									break;
								}
							} catch (Exception e) {}
					if (!found) {
						String tId = ed.item.getTaskName()+"-"+ed.item.getSessionName()+"-"+ed.item.getMuName();
						if (!unexpTasks.contains(tId))
							unexpTasks.add(tId);
					}						
				}
				currentCounters.unexpectedTasks = unexpTasks.size();
				
				//calc missing tasks
				currentCounters.missingTasks = currentCounters.expectedTasks - currentCounters.lateTasks - currentCounters.intimeTasks;
				
			} catch (InterruptedException e) {}
		
		//TODO sort by ...
		
		return list;
	}
	
	//expected tasks - bottom left panel
	public static List<SimulationResult> getExpectedData() {
		return getExpectedData(new TimeWindow());
	}
	
	//expected tasks - bottom left panel
	public static List<SimulationResult> getExpectedData(TimeWindow tw) {				
		ArrayList<SimulationResult> list = new ArrayList<SimulationResult>();
		if (status==AppStatus.READY)
			for (NodeConnection nc : getNodes()) 
				if (nc.isReady()) 
					//get precalculated simulations in requested time window
					list.addAll(nc.getNodeData().getPrecalcSimulations(tw));				
		return list;
	}
	
	public static String getUprocLabel(String node, String uproc) {
		if (uproc==null || uproc.equals(""))
			return "";
		
		NodeConnection nc = nodes.get(node);
		if (nc!=null) 
			return nc.getUprocLabel(uproc);
		return "";
	}

	public static String getSessionLabel(String node, String sess) {		
		if (sess==null || sess.equals(""))
			return "";
		
		NodeConnection nc = nodes.get(node);
		if (nc!=null) 
			return nc.getSessionLabel(sess);
		return "";

	}

	public static LaunchCounters getCurrentCounters() {
		return currentCounters;
	}
	
	public static int getMinutesToNextTimeWindow() {
		if (isConfigured()) {
			Uvms u = getUvms();
			//safety check to avoid loop
			if (u.twLength<5)
				u.twLength=30;
			int min = LocalTime.now().getMinuteOfHour();
			if (min<u.twStartMinute)
				min+=60;
			for (int start = u.twStartMinute; start<= u.twStartMinute+60; start+= u.twLength)
				if (min>=start && min<start+u.twLength) 
					return start+u.twLength - min;
		}
		return 1;
	}

	public static ServletContext getServletContext() {
		return sctx;
	}
}
