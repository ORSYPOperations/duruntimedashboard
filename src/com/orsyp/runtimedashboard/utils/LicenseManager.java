package com.orsyp.runtimedashboard.utils;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;
import java.io.File;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import org.apache.commons.codec.binary.Base64;

import com.orsyp.runtimedashboard.AppManager;

public class LicenseManager {
	
	private static String license = null;

	public static boolean isAuthorized() {
		if (license!=null)
			return true;
		
		try {
			String filename = AppManager.getServletContext().getRealPath("")+File.separator+"WEB-INF/config/license";
			if (!(new File(filename).exists())) {
				System.out.println("License file not found: "+filename);
				return false;
			}
			System.out.println("Loading license file '"+filename+"' ...");
			license = new Scanner( new File(filename) ).useDelimiter("\\A").next();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		
		if (license==null)
			return false;
		
		license = license.trim();
		
		return isValidLicense(license);
	}
	
	public static boolean isValidLicense(String licenseStr) {
		try {
			DESKeySpec keySpec = new DESKeySpec("peppebello".getBytes("UTF8"));
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			SecretKey key = keyFactory.generateSecret(keySpec);
		
			byte[] cleartext = licenseStr.getBytes("UTF8");
			byte[] encrypedPwdBytes = Base64.decodeBase64(cleartext);

			Cipher cipher = Cipher.getInstance("DES"); 
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte[] plainTextPwdBytes = cipher.doFinal(encrypedPwdBytes);
			String expireStr = new String (plainTextPwdBytes);
			
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
	        Date dateStr = formatter.parse(expireStr);
	        
	        Calendar now = Calendar.getInstance();
	        Calendar exp = Calendar.getInstance();
	        exp.setTime(dateStr);
	        //to eliminate the effect of the time the license was created
	        exp.set(Calendar.HOUR_OF_DAY, 23);
	        exp.set(Calendar.MINUTE, 59);
	        
	        return now.before(exp);
	        
		} catch (Exception e) {
			e.printStackTrace();
		} 		
		return false;
	}

	public static void saveLicense(String licenseStr) {
		try {
			String filename = AppManager.getServletContext().getRealPath("")+File.separator+"WEB-INF/config/license";
			PrintWriter wr = new PrintWriter(filename);
			wr.print(licenseStr);
			wr.close();
			license = licenseStr;
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
	
}
