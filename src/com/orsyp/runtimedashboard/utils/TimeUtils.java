package com.orsyp.runtimedashboard.utils;

import java.util.concurrent.TimeUnit;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class TimeUtils {
	
	//current local time zone difference to UTC time
	// > 0 - local time ahead
	public final static int UTCOffsetHours = DateTimeZone.getDefault().getOffset(DateTime.now().getMillis()) / 3600000;
	
	public static String getCurrentTimeZoneOffset() {
	    DateTimeZone tz = DateTimeZone.getDefault();
	    Long instant = DateTime.now().getMillis();

	    String name = tz.getName(instant);

	    long offsetInMilliseconds = tz.getOffset(instant);
	    long hours = TimeUnit.MILLISECONDS.toHours( offsetInMilliseconds );
	    String offset = Long.toString( hours );

	    return name + " (" + offset + " Hours)";
	    // Example: "Mountain Standard Time (-7 Hours)"
	}	

}
