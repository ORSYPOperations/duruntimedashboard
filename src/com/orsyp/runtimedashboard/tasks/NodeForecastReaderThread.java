package com.orsyp.runtimedashboard.tasks;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import com.orsyp.UniverseException;
import com.orsyp.api.task.TaskId;
import com.orsyp.runtimedashboard.api.NodeConnection;
import com.orsyp.runtimedashboard.api.NodeConnection.NodeConnStatus;
import com.orsyp.runtimedashboard.model.SimulationResult;
import com.orsyp.runtimedashboard.model.TimeWindow;

public class NodeForecastReaderThread extends Thread {
	
	private NodeConnection nconn;
	private TimeWindow tw = null;
	private LocalDate date = null;
	private List<SimulationResult> list = new ArrayList<SimulationResult>();

	public NodeForecastReaderThread(NodeConnection nconn, TimeWindow tw) {
		super();
		this.nconn = nconn;
		this.tw = tw;
		setName("Forecast-"+nconn.getNodeName());
	}

	public NodeForecastReaderThread(NodeConnection nconn, LocalDate date) {
		super();
		this.nconn = nconn;
		this.date = date;
	}
	
	public void run() {
		try {
			LocalTime time1 = LocalTime.now();
			List<TaskId> ids = nconn.getTaskIds();
			System.out.println("Node " + nconn.getNodeName() + " - Running forecast on " +ids.size()+ " tasks...");
			for (TaskId id : ids)
				try {
					if (tw!=null)
						list.addAll(nconn.getSimulation(tw, id));
					else
					if (date!=null)
						list.addAll(nconn.getSimulation(date, id));
				} catch (UniverseException ue) {
					//check disconnection					
					if (ExceptionUtils.getRootCause(ue) instanceof com.orsyp.std.ExpiredSessionException) {
						System.out.println("Session expired, reconnectiong in 10 seconds...");
						try {
							Thread.sleep(10000);
						} catch (InterruptedException e) { }
						nconn.reconnect();
						run();
					}			
					else
						ue.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
						
			if (date!=null) {
				if (date.isEqual(new LocalDate())) 
					nconn.getNodeData().setCurrentSimulation(list);
				else
					nconn.getNodeData().setNextSimulation(list);
			}
			else
				nconn.getNodeData().setCurrentSimulation(list);
			LocalTime time2 = LocalTime.now();
			nconn.setStatus(NodeConnStatus.READY);
			System.out.println("Node " + nconn.getNodeName() + " - Forecasted " +list.size()+ " tasks - ms " + time2.minusMillis(time1.getMillisOfDay()).getMillisOfDay());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
	public List<SimulationResult> getList() {
		return list;
	}
	
}
