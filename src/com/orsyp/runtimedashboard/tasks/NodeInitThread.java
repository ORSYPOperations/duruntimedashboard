package com.orsyp.runtimedashboard.tasks;

import com.orsyp.runtimedashboard.api.NodeConnection;
import com.orsyp.runtimedashboard.api.NodeConnection.NodeConnStatus;

public class NodeInitThread extends Thread {
	
	private NodeConnection nconn;
	
	public NodeInitThread(NodeConnection nconn) {
		super();
		this.nconn = nconn;
		setName("Data Reader-" + nconn.getNodeName());
	}

	
	public void run() {
		try {
			nconn.setStatus(NodeConnStatus.INITIALIZING);
			nconn.loadObjects();
			nconn.setStatus(NodeConnStatus.FORECASTING);
		} catch (Exception e) {
			nconn.setStatus(NodeConnStatus.UNREACHABLE);
			e.printStackTrace();
		}
	}	

}
