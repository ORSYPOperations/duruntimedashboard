package com.orsyp.runtimedashboard.tasks;

import javax.servlet.ServletContext;

import com.orsyp.runtimedashboard.AppManager;

public class StartupTask implements Runnable {
	
	private ServletContext ctx;
	
	public StartupTask(ServletContext ctx) {
		super();
		this.ctx = ctx;
	}

	@Override
	public void run() {
		AppManager.init(ctx);
	}
}
