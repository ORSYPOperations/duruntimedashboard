package com.orsyp.runtimedashboard.tasks;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContext;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Minutes;

import com.orsyp.runtimedashboard.AppManager;

public class ForecastTask  implements Runnable {
	
	private ServletContext ctx;
	private ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
	
	public ForecastTask(ServletContext ctx) {
		super();
		this.ctx = ctx;
	}

	@Override
	public void run() {		
		LocalDate tomorrow = new LocalDate().plusDays(1);
		AppManager.calcDayForecasts(tomorrow);
		
		//reschedule schedule for tomorrow at 23:20
		//this avoids problems with daylight savings time changes
		DateTime now = new DateTime();        
		DateTime nextRun = now.plusDays(1).withHourOfDay(23).withMinuteOfHour(20);
	    int minutes = Minutes.minutesBetween(now, nextRun).getMinutes();
	    
	    scheduler.schedule(new ForecastTask(ctx), minutes, TimeUnit.MINUTES);
	    System.out.println("Next forecast scheduled " + nextRun.toString());
	}
}
