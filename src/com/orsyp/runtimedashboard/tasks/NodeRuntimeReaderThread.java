package com.orsyp.runtimedashboard.tasks;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;

import com.orsyp.UniverseException;
import com.orsyp.api.execution.ExecutionItem;
import com.orsyp.runtimedashboard.api.NodeConnection;
import com.orsyp.runtimedashboard.model.NodeData;
import com.orsyp.runtimedashboard.model.TimeWindow;

public class NodeRuntimeReaderThread extends Thread {
	
	private NodeConnection nconn;
	private TimeWindow tw;
	private List<ExecutionItem> list = new ArrayList<ExecutionItem>();
	private List<ExecutionItem> allRunningList = new ArrayList<ExecutionItem>();
	private boolean showAllRunning;

	public NodeRuntimeReaderThread(NodeConnection nconn, TimeWindow tw, boolean showAllRunning) {
		super();
		this.nconn = nconn;
		this.tw = tw;
		this.showAllRunning = showAllRunning;
		setName("ReadRunning-"+nconn.getNodeName());
	}

	
	public void run() {
		try {
			list = nconn.getLaunches(tw);
			if (showAllRunning) 
				allRunningList = nconn.getRunningLaunches();
		} catch (UniverseException ue) {
			//check disconnection
			if (ExceptionUtils.getRootCause(ue) instanceof com.orsyp.std.ExpiredSessionException) {
				System.out.println("Session expired, reconnectiong in 10 seconds...");
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) { }
				nconn.reconnect();
				run();
			}			
			else
				ue.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
	public List<ExecutionItem> getList() {
		return list;
	}
	
	public List<ExecutionItem> getRunningList() {
		return allRunningList;
	}

	public NodeData getNodeData() {
		return nconn.getNodeData();
	}
	
	public NodeConnection getNode() {
		return nconn;
	}
}
