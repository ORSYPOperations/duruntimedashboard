package com.orsyp.runtimedashboard.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;

import com.orsyp.Area;
import com.orsyp.Environment;
import com.orsyp.Identity;
import com.orsyp.api.Client;
import com.orsyp.api.Context;
import com.orsyp.api.central.UniCentral;
import com.orsyp.api.central.services.INetworkNodeService;
import com.orsyp.central.alm.ALMConstants;
import com.orsyp.central.jpa.jpo.NetworkNodeEntity;
import com.orsyp.central.jpa.jpo.NodeInfoEntity;
import com.orsyp.comm.client.ClientServiceLocator;
import com.orsyp.std.ClientConnectionManager;
import com.orsyp.std.MultiCentralConnectionFactory;
import com.orsyp.std.central.UniCentralStdImpl;

public class UvmsConnection {

	private UniCentral cent;
	private String host;
	private int port;
	private String user;
	private String password;
	
	public UvmsConnection (String host, int port, String user, String password) {
		this.host=host;
		this.password=password;
		this.port=port;
		this.user=user;
		
		try {
			createConnection();
			
			if (ClientConnectionManager.getDefaultFactory() == null) 
	            ClientConnectionManager.setDefaultFactory(MultiCentralConnectionFactory.getInstance());

		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void createConnection() throws Exception {
		cent = new UniCentral(host, port);
		cent.setImplementation(new UniCentralStdImpl(cent));
		
		Context ctx = new Context(new Environment("UJCENT",host), new Client(new Identity(user, password, host, "")));
		ctx.setProduct(com.orsyp.api.Product.UNICENTRAL);
		ctx.setUnijobCentral(cent);
		ClientServiceLocator.setContext(ctx);

		cent.login(user, password);
	}
	
	public TreeMap<String,String> getNodeList(String company, String area) {
		TreeMap<String,String> list = new TreeMap<String,String>(); 
		Area a = Area.Exploitation;
		if (Arrays.asList("A", "APP").contains(area.toUpperCase()))
			a = Area.Application;
		else
		if (Arrays.asList("I", "INT").contains(area.toUpperCase()))
			a = Area.Integration;
		else
		if (Arrays.asList("S", "SIM").contains(area.toUpperCase()))
			a = Area.Simulation;
		
		if (ClientConnectionManager.getDefaultFactory() == null) 
            ClientConnectionManager.setDefaultFactory(MultiCentralConnectionFactory.getInstance());

		INetworkNodeService nns = ClientServiceLocator.getNetworkNodeService();
		List<NetworkNodeEntity> duNodes = nns.getAllNodes((long) 0);
		
		List<String> selectedNodeNames = new ArrayList<String>(); 
		NodeInfoEntity[] nnes = ClientServiceLocator.getNodeInfoService().getAllNodeInfoFromCache(-1, null);
		
		for (NodeInfoEntity nie : nnes)
			if (nie.getProductCode().equals("DUN"))
				if (company.equals(nie.getCompany())) 
					if (nie.getNodeStatusForArea(a)==ALMConstants.STATUS_CONNECTED) 						
						selectedNodeNames.add(nie.getNodeName());
		
		for (NetworkNodeEntity nne : duNodes) 
			if (selectedNodeNames.contains(nne.getName()))
				list.put(nne.getName(),nne.firstHost().getName());
		
		return list;
	}
	
	
	public UniCentral getUniCentral() {
		if (cent==null)
			try {
				createConnection();
			} catch (Exception e) {
				e.printStackTrace();
			}
		return cent;
	}

	public String getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}
}
