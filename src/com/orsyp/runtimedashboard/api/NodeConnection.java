package com.orsyp.runtimedashboard.api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import com.orsyp.Area;
import com.orsyp.Environment;
import com.orsyp.Identity;
import com.orsyp.SyntaxException;
import com.orsyp.UniverseException;
import com.orsyp.api.Client;
import com.orsyp.api.Context;
import com.orsyp.api.Product;
import com.orsyp.api.central.UniCentral;
import com.orsyp.api.execution.ExecutionFilter;
import com.orsyp.api.execution.ExecutionItem;
import com.orsyp.api.execution.ExecutionList;
import com.orsyp.api.execution.ExecutionStatus;
import com.orsyp.api.hr.HrStatisticFilter;
import com.orsyp.api.hr.HrStatisticItem;
import com.orsyp.api.hr.HrStatisticList;
import com.orsyp.api.security.Operation;
import com.orsyp.api.session.SessionFilter;
import com.orsyp.api.session.SessionItem;
import com.orsyp.api.session.SessionList;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.api.task.Simulation;
import com.orsyp.api.task.Task;
import com.orsyp.api.task.TaskFilter;
import com.orsyp.api.task.TaskId;
import com.orsyp.api.task.TaskItem;
import com.orsyp.api.task.TaskList;
import com.orsyp.api.task.TaskType;
import com.orsyp.api.uproc.UprocFilter;
import com.orsyp.api.uproc.UprocItem;
import com.orsyp.api.uproc.UprocList;
import com.orsyp.owls.impl.execution.OwlsExecutionListImpl;
import com.orsyp.owls.impl.hr.OwlsStatisticListImpl;
import com.orsyp.owls.impl.session.OwlsSessionListImpl;
import com.orsyp.owls.impl.task.OwlsTaskImpl;
import com.orsyp.owls.impl.task.OwlsTaskListImpl;
import com.orsyp.owls.impl.uproc.OwlsUprocListImpl;
import com.orsyp.runtimedashboard.model.NodeData;
import com.orsyp.runtimedashboard.model.SimulationResult;
import com.orsyp.runtimedashboard.model.TaskData;
import com.orsyp.runtimedashboard.model.TimeWindow;

public class NodeConnection {
	
	public enum NodeConnStatus {UNINITIALIZED, INITIALIZING, FORECASTING, READY, UNREACHABLE};
	
	private Context context;	
	private NodeData nodeData = new NodeData();
	private String nodeName;
	private String hostName;
	
	private NodeConnStatus status = NodeConnStatus.UNINITIALIZED;
		
	private String company;
	private Area area;
	private String user;
	private UniCentral central;
	
	
	public NodeConnection(UvmsConnection conn, String company, String areaName, String node, String hostName) {
		this.company = company;
		this.user = conn.getUser(); 
		this.nodeName = node;
		this.hostName = hostName;
		this.central = conn.getUniCentral();
		
		area = Area.Exploitation;
		if (Arrays.asList("A", "APP").contains(areaName.toUpperCase()))
			area = Area.Application;
		else
		if (Arrays.asList("I", "INT").contains(areaName.toUpperCase()))
			area = Area.Integration;
		else
		if (Arrays.asList("S", "SIM").contains(areaName.toUpperCase()))
			area = Area.Simulation;
				
		try {
			context = makeContext(conn.getUniCentral(), node, company, area, conn.getUser());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		
	
	private Context makeContext(UniCentral central, String node, String company, Area area, String user) throws SyntaxException {
		Context ctx = null;
		Client client = new Client(new Identity(user, "", node, ""));
		ctx = new Context(new Environment(company, node, area), client, central);
		ctx.setProduct(Product.OWLS);
		return ctx;
	}
		
	public Context getContext() {
		return context;
	}
	
	public String getNodeName() {		
		return nodeName;		
	}
	
	public String getHostName() {		
		return hostName;
	}	
	
	
	//-------------------------------------------
	
	public void loadObjects() throws UniverseException {
		LocalTime start = LocalTime.now();
		System.out.println("Node " + nodeName + " - Loading objects...");
		nodeData.clear();
		
		//uprocs		
        UprocList ulist = new UprocList(getContext(), new UprocFilter());
        ulist.setImpl(new OwlsUprocListImpl());
        ulist.extract();
        for (int i = 0; i < ulist.getCount(); i++) {
			UprocItem it = ulist.get(i);
			nodeData.addUproc(it.getName(), it.getLabel());
		}
        
        //sessions and session structure
		SessionList slist = new SessionList(getContext(), new SessionFilter());
		slist.setImpl(new OwlsSessionListImpl());
		slist.extract();
		for (int i = 0; i < slist.getCount(); i++) {
			SessionItem it = slist.get(i);
			nodeData.addSession(it.getName(), it.getLabel());
			
			/*
			Session s = new Session(getContext(), new SessionId(it.getName(), it.getVersion()));
            s.setImpl(new OwlsSessionImpl());
            s.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
            s.extract();
            
            nodeData.addSessionUprocs(it.getName(),s.getUprocs());
            */
		}
		
		//tasks		
		TaskFilter filter = new TaskFilter();
        filter.setTemplate(false);    
        filter.setType(TaskType.Scheduled);
        TaskList tlist = new TaskList(getContext(), filter);
        tlist.setImpl(new OwlsTaskListImpl());
        tlist.extract();				
        for (int i = 0; i < tlist.getCount(); i++) {
			TaskItem it = tlist.get(i);
			nodeData.addTask(it.getIdentifier().toString(), it.getIdentifier());
        }
        
        //executions
		ExecutionList list = new ExecutionList(getContext(), new ExecutionFilter());
		list.setImpl(new OwlsExecutionListImpl());
		list.extract(Operation.DISPLAY);
		
		for (int idx = 0; idx<list.getCount(); idx++) 
			nodeData.addExecution(list.get(idx));		
		
		//uproc stats
		List<HrStatisticItem> stats = getAllStats();
		nodeData.addStats(stats);		
		
		LocalTime step1 = LocalTime.now();
		System.out.println("Node " + nodeName + " - Loading done - ms: " + step1.minusMillis(start.getMillisOfDay()).getMillisOfDay());
		
		//build uproc estimated start times and durations
		nodeData.calcStartTimes();
		
		//build task estimated durations from stats		
		nodeData.calcTaskDurations();
		
		System.out.println("Node " + nodeName + " - Stats processing done - ms: " + LocalTime.now().minusMillis(step1.getMillisOfDay()).getMillisOfDay());
	}
	
	public String getUprocLabel(String uproc) {
		return nodeData.getUprocLabel(uproc);
	}
	
	public String getSessionLabel(String sess) {
		return nodeData.getSessionLabel(sess);
	}
	
	public List<TaskId> getTaskIds () {		
		return nodeData.getTaskIds();
	}
	
	//-------------------------------------------	

	
	public List<ExecutionItem> getLaunches(TimeWindow tw)  throws Exception {
		//LocalTime time1 = LocalTime.now();
		//System.out.println("Node " + nodeName + " - Reading running uprocs...");
		List<ExecutionItem> results = new ArrayList<ExecutionItem>();
				
		ExecutionFilter filter = new ExecutionFilter();
		filter.setStatuses(new ExecutionStatus[] {ExecutionStatus.Running, 
				ExecutionStatus.Completed, ExecutionStatus.Aborted, ExecutionStatus.CompletionInProgress  });
		DateTime start = tw.getStart().toDateTimeToday();
		DateTime end = tw.getEnd().toDateTimeToday();
		filter.setBeginDate(new SimpleDateFormat("yyyyMMdd").format(start.toDate()));
		filter.setEndDate(new SimpleDateFormat("yyyyMMdd").format(end.toDate()));		
		filter.setBeginHour(new SimpleDateFormat("HHmmss").format(start.toDate()));
		filter.setEndHour(new SimpleDateFormat("HHmmss").format(end.toDate()));
		
		ExecutionList list = new ExecutionList(getContext(), filter);
		list.setImpl(new OwlsExecutionListImpl());
		list.extract(Operation.DISPLAY);
		
		for (int idx = 0; idx<list.getCount(); idx++) 
			results.add(list.get(idx));
				
		//LocalTime time2 = LocalTime.now();		
		//System.out.println("Node " + nodeName + " - " + results.size() + " running uprocs. - ms: " + time2.minusMillis(time1.getMillisOfDay()).getMillisOfDay());
		return results;
	}
	
	public List<ExecutionItem> getRunningLaunches()   throws Exception {
		List<ExecutionItem> results = new ArrayList<ExecutionItem>();
		
		ExecutionFilter filter = new ExecutionFilter();
		filter.setStatuses(new ExecutionStatus[] {ExecutionStatus.Running});
		DateTime end = DateTime.now();
		filter.setBeginDate("20000101");
		filter.setEndDate(new SimpleDateFormat("yyyyMMdd").format(end.toDate()));		
		filter.setBeginHour("000000");
		filter.setEndHour(new SimpleDateFormat("HHmmss").format(end.toDate()));		
		ExecutionList list = new ExecutionList(getContext(), filter);
		list.setImpl(new OwlsExecutionListImpl());
		list.extract(Operation.DISPLAY);
		
		for (int idx = 0; idx<list.getCount(); idx++) 
			results.add(list.get(idx));				
		return results;
	}	

	public List<SimulationResult> getSimulation(TimeWindow tw, TaskId id)  throws Exception {
		DateTime start = tw.getStart().toDateTimeToday();
		DateTime end = tw.getEnd().toDateTimeToday();
         
		return getSimulationData(start, end, id, tw);
	}	
	
	public List<SimulationResult> getSimulation(LocalDate date, TaskId id) throws Exception {		
		DateTime start = date.toDateTime(new LocalTime().withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0));
		DateTime end = start.plusDays(1);		
         
		return getSimulationData(start, end, id, null);
	}
	

	private List<SimulationResult> getSimulationData(DateTime start, DateTime end, TaskId id, TimeWindow tw)  throws Exception {
		ArrayList<SimulationResult> res = new ArrayList<SimulationResult>();
		
		System.out.println("Forecasting task: " + id );		
        
        long avgDuration = -1; 
    	LocalTime estimatedTime = null;
    	TaskData taskData = nodeData.getTask(id);
        if (taskData!=null) {
        	if (taskData.hasDurationStats())
        		avgDuration = taskData.getAverageDurationMSecs();
        	if (taskData.hasStartEstimation())
        		estimatedTime = taskData.getStartTime(LocalDate.now().getDayOfWeek());
        	else
        		System.out.println(" Task has no estimated start time!");
		}
        else {        	
        	System.out.println(" Task data not found!");
        	return res;
        }
		
        //run simulation only if checking the whole day or if the estimated time exists
        if (taskData.hasStartEstimation())
	        if (tw==null || tw.includes(estimatedTime)) {
	        	Task obj = new Task(getContext(), id);
	            obj.setImpl(new OwlsTaskImpl());
	            obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance ());        
	            obj.extract();
	            
		        Simulation s = obj.simulateDay(start.toDate(), end.toDate(), Operation.SIMULATE);
		        if ( s.getForecastDates().length>0) {
		        	System.out.println("Task forecasted: " + id);
		        	SimulationResult ex = new SimulationResult();
		        	ex.task = id.getName(); 
		        	ex.mu = id.getMuName();
		        	ex.session = id.getSessionName();
		        	ex.uproc = id.getUprocName();
		        	ex.node = nodeName;
		        	ex.host = hostName;
		            
		        	ex.estimatedStartTime = estimatedTime;
		        	ex.setAvgDuration(avgDuration);
		        	
		        	res.add(ex);
		    	}
		        else
		        	System.out.println("Task not running today: " + id);
	        }
         
		return res;
	}
	
	
	public List<HrStatisticItem> getAllStats() throws UniverseException {
		return getStats(null);
	}
	
	public List<HrStatisticItem> getStats(String uprocName) throws UniverseException {
		HrStatisticFilter filt = new HrStatisticFilter();
		if (uprocName!=null)
			filt.setUprocName(uprocName);
		HrStatisticList list = new HrStatisticList(getContext(),filt);
		list.setImpl(new OwlsStatisticListImpl());
		list.extract();
		
		ArrayList<HrStatisticItem> res = new  ArrayList<HrStatisticItem>();
		for (int idx = 0; idx<list.getCount(); idx++) 
			res.add(list.get(idx));
		
		return res;
	}
	
	public NodeData getNodeData() {
		return nodeData;
	}


	public NodeConnStatus getStatus() {
		return status;
	}


	public void setStatus(NodeConnStatus status) {
		this.status = status;
	}


	public boolean isReady() {
		return status==NodeConnStatus.READY;
	}


	public void reconnect() {
		try {
			makeContext(central, nodeName, company, area, user);
		} catch (SyntaxException e) {
			e.printStackTrace();
		}		
	}

}
