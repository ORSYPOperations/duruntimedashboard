package com.orsyp.runtimedashboard.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orsyp.runtimedashboard.AppManager;
import com.orsyp.runtimedashboard.model.LaunchCounters;

/**
 * Servlet implementation class GetCounters
 */
public class GetCounters extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetCounters() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter w = response.getWriter();
		response.setHeader("", "");
		LaunchCounters ct = AppManager.getCurrentCounters();
		w.println(	ct.intimeTasks+","+
					ct.lateTasks+","+
					ct.missingTasks+","+
					ct.unexpectedTasks+","+
					ct.expectedTasks+","+
					ct.abortedUprocs+","+
					ct.runningUprocs);
	}

}
