package com.orsyp.runtimedashboard.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orsyp.runtimedashboard.utils.LicenseManager;


public class SaveLicense extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SaveLicense() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String license = request.getParameter("license");
		
		if (LicenseManager.isValidLicense(license))	
			try {			
				LicenseManager.saveLicense(license);
				response.sendRedirect("configuration.jsp");
			} catch (Exception e) {
				e.printStackTrace();
				throw new ServletException(e);
			}		
		else
			response.sendRedirect("unauthorized.html");
	}

}
