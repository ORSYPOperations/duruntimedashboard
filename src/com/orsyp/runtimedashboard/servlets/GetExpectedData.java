package com.orsyp.runtimedashboard.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.joda.time.format.DateTimeFormat;

import com.orsyp.runtimedashboard.AppManager;
import com.orsyp.runtimedashboard.api.NodeConnection.NodeConnStatus;
import com.orsyp.runtimedashboard.model.SimulationResult;


/**
 * Servlet implementation class GetExpectedData
 */
public class GetExpectedData extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetExpectedData() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter w = response.getWriter();
		response.setHeader("", "");
		
		if (AppManager.getNodeCount(NodeConnStatus.INITIALIZING) + 
			AppManager.getNodeCount(NodeConnStatus.FORECASTING) +
			AppManager.getNodeCount(NodeConnStatus.UNINITIALIZED) >0 ) 
			w.print("<tr><td colspan='5' align='center'>Some nodes are still initializing...</td></tr>");
		else {
		
			List<SimulationResult> data = AppManager.getExpectedData();
			int i=0;
			for (SimulationResult it : data) {
				String upr = it.uproc;
				String sess = it.session;
				String node = it.node;	
				String host = it.host;
				String uprLabel = StringEscapeUtils.escapeHtml4(AppManager.getUprocLabel(node, upr));
				String sessLabel = StringEscapeUtils.escapeHtml4(AppManager.getSessionLabel(node, sess));
				String startTime = it.estimatedStartTime.toString(DateTimeFormat.forPattern("HH:mm"));
				String finishTime = it.getFinishTime();
				
				String classAndTitle="";
				String missingStr="";
				boolean missing = i == 2;
				if (missing) {
					classAndTitle = "class='missing show-tooltip' title='Missing'";
					missingStr = "<i class='icon-warning-sign'></i> ";
				}
				w.print("<tr "+classAndTitle+">");			
				w.print("<td>" + node + " <span class='duLabel'>" + host+ "</span></td>");
				w.print("<td>" + sess + " <span class='duLabel'>" + sessLabel+ "</span></td>");
				w.print("<td>" + missingStr + upr + " <span class='duLabel'>"+uprLabel+"</span></td>");
				w.print("<td>" + startTime + "</td>");
				w.print("<td>" + finishTime + "</td>");
				w.print("</tr>");	
			}
			
			if (data.size()==0) 
				w.print("<tr><td colspan='5' align='center'>No expected runs</td></tr>");
		}
	}

}
