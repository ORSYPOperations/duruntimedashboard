package com.orsyp.runtimedashboard.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import com.orsyp.api.execution.ExecutionItem;
import com.orsyp.runtimedashboard.AppManager;
import com.orsyp.runtimedashboard.model.ExecutionData;

/**
 * Servlet implementation class GetRunningData
 */
public class GetRunningData extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetRunningData() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter w = response.getWriter();
		boolean showAll = request.getParameter("all").equalsIgnoreCase("true");
		response.setHeader("", "");
		List<ExecutionData> data = AppManager.getRunningData(showAll);
		
		for (ExecutionData d : data) {
			ExecutionItem it = d.item;
			String classAndTitle="";
			String lateStr="";
			String unexpStr="";
			
			if (d.isLate()) {
				classAndTitle = "class='late show-tooltip' title='Late'";
				lateStr = " <i class='icon-time'></i>";
			}
			else
			if (d.isUnexpected()) {
				classAndTitle = "class='unexpected show-tooltip' title='Unexpected'";
				unexpStr ="<i class='icon-warning-sign'></i> ";
			}
			
			String upr = it.getUprocName();
			String sess = it.getSessionName();
			String node = it.getNodeName();
			String host = d.host;	
			String uprLabel = StringEscapeUtils.escapeHtml4(AppManager.getUprocLabel(node, upr));
			String sessLabel = StringEscapeUtils.escapeHtml4(AppManager.getSessionLabel(node, sess));
			
				
			w.print("<tr "+classAndTitle+">");			
			w.print("<td>" + node + " <span class='duLabel'>" + host+ "</span></td>");
			w.print("<td>" + sess + " <span class='duLabel'>" + sessLabel + "</span></td>");
			w.print("<td>" + unexpStr + upr + " <span class='duLabel'>" + uprLabel + " </span></td>");
			w.print("<td>" + d.startStr + "</td>");
			w.print("<td>" + d.durationStr + lateStr +"</td>");
			w.print("</tr>");
		}
		
		if (data.size()==0) {
			w.print("<tr><td colspan='5' align='center'>No running Uprocs</td></tr>");
		}
	}

}

