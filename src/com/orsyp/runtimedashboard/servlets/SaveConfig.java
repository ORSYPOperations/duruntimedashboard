package com.orsyp.runtimedashboard.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orsyp.runtimedashboard.AppManager;
import com.orsyp.runtimedashboard.model.Uvms;

/**
 * Servlet implementation class SaveConfig
 */
public class SaveConfig extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaveConfig() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final Uvms u = new Uvms();
		u.area = request.getParameter("area");
		u.company = request.getParameter("company");
		u.server = request.getParameter("server");
		u.user = request.getParameter("user");
		u.password = request.getParameter("password");
		u.port = Integer.parseInt(request.getParameter("port"));
		
		try {
			u.twLength = Integer.parseInt(request.getParameter("twLength"));
		} catch (Exception e) {
			u.twLength = 30;
		}
		try {
			u.twStartMinute = Integer.parseInt(request.getParameter("startMin"));
		} catch (Exception e) {
			u.twStartMinute = 15;
		}
		
		String[] nodes = request.getParameter("nodes").split("[ ,]+");
		for (String node : nodes) 
			if (node.trim().length()>0)
				u.nodeFilter.add(node.trim());

		(new Thread() {
			  public void run() {
				  try {
					AppManager.saveConfiguration(u);
				} catch (Exception e) {
					e.printStackTrace();
				}
			  }
		}).start();			
		response.sendRedirect("dashboard.jsp");
	}

}
