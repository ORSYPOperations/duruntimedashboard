package com.orsyp.runtimedashboard.test;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;

import com.orsyp.runtimedashboard.AppManager;
import com.orsyp.runtimedashboard.api.NodeConnection;
import com.orsyp.runtimedashboard.model.NodeData;
import com.orsyp.runtimedashboard.model.SimulationResult;
import com.orsyp.runtimedashboard.model.TaskData;

/**
 * Servlet implementation class TestForecasts
 */
public class TestForecasts extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public TestForecasts() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);	
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter w = response.getWriter();
		w.println("<!DOCTYPE HTML><html><body>");
		
		String filter = request.getParameter("node");		
		
		List<NodeConnection> nodes = AppManager.getNodes();
		w.println("<h2>");
		w.println("Nodes: " +nodes.size());
		w.println("</h2>");
		
		
		for (NodeConnection n:nodes)
			if (filter.length()==0 || n.getNodeName().equals(filter)) {
				NodeData d = n.getNodeData();
				w.println("<h3>");
				w.println("Node " +n.getNodeName() + " - " + n.getStatus().name());
				w.println("</h3>");
				w.println("<p>");
				w.println("Tasks: " + n.getTaskIds().size() + " - Sessions: " + d.getSessions().size() + " - Uprocs: " + d.getUprocs().size());
				w.println("</p>");
				w.println("<p>");
				w.println("Forecasted tasks:");
				w.println("</p>");
				w.println("<pre>");				
				for (SimulationResult s: d.getAllSimulations()) {
					w.println("Task: "+ s.task + " - Session: " + s.session + " - MU: " + s.mu + " - Uproc: " + s.uproc);
					try {
						w.println("&nbsp;&nbsp;&nbsp;Start: "+ s.estimatedStartTime.toString(DateTimeFormat.forPattern("HH:mm")) + " - End: " + s.getFinishTime()+ " - Duration: " + s.getDurationStr());
					} catch (Exception e) {
						w.println("&nbsp;&nbsp;&nbsp;Start: UNKNOWN - End: " + s.getFinishTime()+ " - Duration: " + s.getDurationStr());
					} 
				}				
				w.println("</pre>");
				w.println("<p>");
				w.println("Task stats and calculated data:");
				w.println("</p>");
				w.println("<pre>");
				
				for (Entry<String,TaskData> entry : d.getTasks().entrySet()) {
					if (entry.getValue().hasStartEstimation()  || entry.getValue().hasDurationStats()) {
						String durStr = " --- Duration UNKNOWN - no stats available";						
						if (entry.getValue().hasDurationStats()) 
							durStr = " --- Duration msec: " + entry.getValue().getAverageDurationMSecs();
						
						String noStartStr = " --- Start time UNKNOWN - no executions available";
						if (entry.getValue().hasStartEstimation()) 
							noStartStr = "";
						
						w.println("Task: "+ entry.getKey() + durStr + noStartStr);
						
						for (int day=1; day<=7; day++) {
							LocalTime startTime = entry.getValue().getStartTime(day);
							if (startTime!=null)
								w.println("&nbsp;&nbsp;&nbsp;Day: "+ entry.getKey() + " - Time: " +  startTime.toString(DateTimeFormat.forPattern("HH:mm")));
						}
					}
					else
						w.println("Task: "+ entry.getKey() + " --- NO ESTIMATION DATA");
				}
				w.println("</pre>");
			}
	}

}
