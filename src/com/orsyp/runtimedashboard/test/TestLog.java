package com.orsyp.runtimedashboard.test;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orsyp.runtimedashboard.AppManager;
import com.orsyp.runtimedashboard.api.NodeConnection;
import com.orsyp.runtimedashboard.model.NodeData;
import com.orsyp.runtimedashboard.model.TaskData;
import com.orsyp.runtimedashboard.model.TimeWindow;

/**
 * Servlet implementation class TestLog
 */
public class TestLog extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestLog() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter w = response.getWriter();
		w.println("<!DOCTYPE HTML><html><body>");
		
		String filter = request.getParameter("node");		
		
		List<NodeConnection> nodes = AppManager.getNodes();
		w.println("<h2>");
		w.println("Nodes: " +nodes.size());
		w.println("</h2>");
		
		
		for (NodeConnection n:nodes)
			if (filter.length()==0 || n.getNodeName().equals(filter)) {
				NodeData d = n.getNodeData();
				w.println("<h3>");
				w.println("Node " +n.getNodeName() + " - " + n.getStatus().name());
				w.println("</h3>");
				w.println("<p>");
				w.println("Tasks: " + n.getTaskIds().size() + " - Sessions: " + d.getSessions().size() + " - Uprocs: " + d.getUprocs().size());
				w.println("</p>");
				w.println("<p>");
				w.println("Past executions: " + d.getExecutions().size() + " - Simulations: " + d.getPrecalcSimulations(new TimeWindow()).size());
				w.println("</p>");
				
				if (request.getParameter("full")!=null && request.getParameter("full").equals("on")) {
					w.println("<p>Tasks:</p><pre>");
					for (TaskData td: d.getTasks().values()) {
						w.println(String.format("%-40s - Mu: %-20s - Session: %-30s - Header: %s", td.id.getName(), td.id.getMuName(), td.id.getSessionName(), td.id.getUprocName()));
					}
					w.println("</pre>");
				}				
			}
	}

}
