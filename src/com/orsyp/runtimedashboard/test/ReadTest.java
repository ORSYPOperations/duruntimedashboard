package com.orsyp.runtimedashboard.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.eclipse.jdt.internal.compiler.ast.ForeachStatement;
import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import com.orsyp.api.execution.ExecutionItem;
import com.orsyp.api.hr.HrStatisticItem;
import com.orsyp.api.security.Operation;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.api.task.DetailedSimulation;
import com.orsyp.api.task.ForecastLaunch;
import com.orsyp.api.task.Simulation;
import com.orsyp.api.task.Task;
import com.orsyp.api.task.TaskId;
import com.orsyp.owls.impl.task.OwlsTaskImpl;
import com.orsyp.runtimedashboard.api.NodeConnection;
import com.orsyp.runtimedashboard.api.UvmsConnection;
import com.orsyp.runtimedashboard.model.SimulationResult;
import com.orsyp.runtimedashboard.model.TimeWindow;
import com.orsyp.util.Run;

public class ReadTest {

	/**
	 * @param args
	 * @throws Exception 
	 */
	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws Exception {
		UvmsConnection uvc = new UvmsConnection("localhost", 4184, "admin", "admin");
		NodeConnection c = new NodeConnection(uvc, "UNIV60", "X", "itlpmps01", "some host name");
		
		List<ExecutionItem> launches = c.getLaunches(new TimeWindow());
		System.out.println("Running: " + launches.size());
		for (ExecutionItem l : launches)
			System.out.println("  " + l.getUprocName());			
		
		/*
		DateTime start = DateTime.now();
		start = start.withMinuteOfHour(0).withHourOfDay(0);
		DateTime end = start.plusDays(1);			
		List<SimulationResult> sims = c.getSimulation(start, end, "TEST1", "ITLPMPS01");
		System.out.println("Forecasted: " + sims.size());
		for (SimulationResult s : sims)
			System.out.println("  " + s.taskName + " " + s.beginDate.toLocaleString());
		*/
		
		Date   begdate = new SimpleDateFormat("dd/MM/yyyy").parse("13/05/2014");
        Date   enddate = new SimpleDateFormat("dd/MM/yyyy").parse("16/05/2014");
		
		Task obj = new Task(c.getContext(), TaskId.createWithName("TEST_THUR", "000", "ITLPMPS01", false));
        obj.setImpl(new OwlsTaskImpl());
        obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance ());
        obj.extract();
        
        Simulation sim = obj.simulateDay(begdate, enddate, Operation.SIMULATE );
        
        for (Date fl : sim.getForecastDates()) {        	
        	System.out.println(fl);
        }
        
        DetailedSimulation ds = obj.simulateFull( begdate, enddate );
        System.out.println("Runs:");
        for( Run r : ds.getRuns() ) {
            System.out.println(r.getTime());
        }
        
        System.out.println("Forecast:");
        for( Date d : ds.getForecastDates() ) {
            System.out.println(d);
        }
        
        System.out.println("Excluded:");
        for( Date d : ds.getExcludedDates() ) {
            System.out.println(d);
        }
		
		List<HrStatisticItem> stats = c.getStats("TEST1");
		System.out.println("Stats: " + stats.size());
		for (HrStatisticItem s : stats) {
			try {
				LocalTime dur = new LocalTime();
				dur = dur.withMillisOfDay((int)s.getUprocStatistic().getExecCompleted().getAverageDuration());
				System.out.println("  " + s.getIdentifier().getUprocName() + "/"+ s.getIdentifier().getMuName() + 
									" - " + s.getUprocStatistic().getExecCompleted().getAverageDuration() + 
									" " +dur.toString());
			}
			catch (Exception e) {}
		}
	}

}
