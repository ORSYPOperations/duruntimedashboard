<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" 
    import="com.orsyp.runtimedashboard.AppManager"
    import="com.orsyp.runtimedashboard.model.Uvms"
    import="com.orsyp.runtimedashboard.utils.LicenseManager"
%>
<%
	if (!LicenseManager.isAuthorized()) {
		response.sendRedirect("unauthorized.html");
		return;
	}
%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Uvms Server Connection</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
    
	<link href="./css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="./css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
	
	<link href="./css/font-awesome.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    
    <link href="./css/jquery-ui-1.10.0.custom.min.css" rel="stylesheet">
    
    <link href="./css/base-admin-3.css" rel="stylesheet">
    <link href="./css/base-admin-3-responsive.css" rel="stylesheet">
    
	<link href="./css/setup.css" rel="stylesheet">

    <link href="./css/custom.css" rel="stylesheet">

</head>

<body>
	
<nav class="navbar navbar-inverse" role="navigation">

	<div class="container">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="index.html">$Universe Monitoring Console</a>
  </div>

</div> <!-- /.container -->
</nav>

<%
	String server = "";
	String port = "";
	String user = "";
	String password = "";
	String company = "";
	String area = "";
	String nodes = "";
	String startMin = "";
	String twLength = "";
	
	String selectedEXP = "";
	String selectedAPP = "";
	String selectedSIM = "";
	String selectedINT = "";
	String selected5 = "";
	String selected10 = "";
	String selected15 = "";
	String selected20 = "";
	String selected30 = "";
	String selected60 = "";
	
	if (AppManager.isConfigured()) {
		Uvms u = AppManager.getUvms();
		server = u.server;
		port = String.valueOf(u.port);
		user = u.user;
		password = u.password;
		company = u.company;
		area = u.area;
		startMin = String.valueOf(u.twStartMinute);
		twLength = String.valueOf(u.twLength);
		
		for (String n: u.nodeFilter) {
			if (nodes.length()>0)
				nodes+= ", ";
			nodes+=n;
		}
	
		if (area.equals("EXP"))
			selectedEXP = "selected";
		else
		if (area.equals("APP"))
			selectedAPP = "selected";
		else
		if (area.equals("INT"))
			selectedINT = "selected";
		else
		if (area.equals("SIM"))
			selectedSIM = "selected";
		
		
		if (twLength.equals("5"))
			selected5 = "selected";
		else
		if (twLength.equals("10"))
			selected10 = "selected";
		else
		if (twLength.equals("15"))
			selected15 = "selected";
		else
		if (twLength.equals("20"))
			selected20 = "selected";
		else
		if (twLength.equals("30"))
			selected30 = "selected";
		else
		if (twLength.equals("60"))
			selected60 = "selected";		
	}
%>

<div class="account-container register stacked">
	
	<div class="content clearfix">
		
		<form action="SaveConfig" method="post">
		
			<h1>Setup Uvms Connection</h1>			
			
			<div class="login-fields">
				
				<p>Insert Uvms server connection parameters</p>
				
				<div class="field">
					<label for="server">Uvms server</label>
					<input type="text" id="server" name="server" value="<%=server%>" placeholder="Uvms server" class="form-control" required/>
				</div> 
				
				<div class="field">
					<label for="port">Port</label>	
					<input type="text" id="port" name="port" value="<%=port%>" placeholder="Port" class="form-control" required/>
				</div> 
				
				
				<div class="field">
					<label for="user">User name</label>
					<input type="text" id="user" name="user" value="<%=user%>" placeholder="User name" class="form-control" required/>
				</div> 
				
				<div class="field">
					<label for="password">Password:</label>
					<input type="password" id="password" name="password" value="" placeholder="Password" class="form-control" required/>
				</div> 
				
				<div class="field">
					<label for="company">Company</label>
					<input type="text" id="company" name="company" value="<%=company%>" placeholder="Company" class="form-control" required/>
				</div> 
				
				<div class="field">
					<label for="area">Area</label>
					<select class="form-control" id="area" name="area">
						<option value="EXP" <%=selectedEXP%>>EXP</option>
						<option value="APP" <%=selectedAPP%>>APP</option>
						<option value="SIM" <%=selectedSIM%>>SIM</option>
						<option value="INT" <%=selectedINT%>>INT</option>
					</select>					
				</div> 
				
				<div class="field">
					<label for="nodes">Node filter</label>
					<input type="text" id="nodes" name="nodes" value="<%=nodes%>" placeholder="Node filter (empty to select all nodes)" class="form-control"/>
				</div>
				
				<div class="field">
					<label for="startMin">Time window start minute</label>
					<input type="text" id="startMin" name="startMin" value="<%=startMin%>" placeholder="Time window start minute" class="form-control" required/>
				</div> 
				
				<div class="field">
					<label for="twLength">Time window length</label>
					<select class="form-control" id="twLength" name="twLength">
						<option value="5" <%=selected5%>>5</option>
						<option value="10" <%=selected10%>>10</option>
						<option value="15" <%=selected15%>>15</option>
						<option value="20" <%=selected20%>>20</option>
						<option value="30" <%=selected30%>>30</option>
						<option value="60" <%=selected60%>>60</option>
					</select>					
				</div> 
				
			</div> <!-- /login-fields -->
			
			<div class="login-actions">
				<button class="login-action btn btn-primary">Save configuration</button>
				
			</div> <!-- .actions -->
			
		</form>
		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->

<div class="footer navbar-fixed-bottom">
		
	<div class="container">
		
		<div class="row">
			
			<div id="footer-copyright" class="col-md-6">
				&copy; 2014 Orsyp
			</div> <!-- /span6 -->
			
			<div id="footer-terms" class="col-md-6">
				<a href="http://www.orsyp.com" target="_blank">Orsyp corporate site</a>
			</div> <!-- /.span6 -->
			
		</div> <!-- /row -->
		
	</div> <!-- /container -->
	
</div> <!-- /footer -->

<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="./js/jquery-1.9.1.min.js"></script>
<script src="./js/jquery-ui-1.10.0.custom.min.js"></script>
<script src="./js/bootstrap.min.js"></script>

<script src="./js/Application.js"></script>

<script src="./js/setup.js"></script>

</body>
</html>
