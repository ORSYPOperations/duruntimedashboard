<%@page import="com.orsyp.runtimedashboard.utils.LicenseManager"%>
<%@page import="com.orsyp.runtimedashboard.api.NodeConnection.NodeConnStatus"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" 
    import="com.orsyp.runtimedashboard.AppManager"
    import="com.orsyp.runtimedashboard.model.*"
%>
<%
	if (!LicenseManager.isAuthorized()) {
		response.sendRedirect("unauthorized.html");
		return;
	}

	Uvms u = AppManager.getUvms();
	String server = u.server;
	String port = String.valueOf(u.port);
	String user = u.user;
	String password = u.password;
	String company = u.company;
	String area = u.area;	
	
	int tot = AppManager.getNodes().size();	
	int ready = AppManager.getNodeCount(NodeConnStatus.READY);

	String nodes = ""+tot;
	if (ready!=tot) {
		nodes= ""+ready+"/"+tot + " (";	
		int nl = AppManager.getNodeCount(NodeConnStatus.INITIALIZING) + AppManager.getNodeCount(NodeConnStatus.FORECASTING);
		if (nl>0)
			nodes += ""+nl+" loading";
		int ne = AppManager.getNodeCount(NodeConnStatus.UNREACHABLE);
		if (ne>0) {
			if (nl>0)
				nodes += ", ";
			nodes += ""+ne+" disconnected";
		}
		nodes += ")";
	}
	
	if (!AppManager.isConnectedToUvms()) {
		nodes = "0<br><span style='color:red;font-size:14px'>UVMS Connection ERROR</span>";
	}
	
	String window = new TimeWindow().asString();
%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>$Universe Monitoring Console</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="./css/font-awesome.min.css" rel="stylesheet">        
    
    <link href="./css/jquery-ui-1.10.0.custom.min.css" rel="stylesheet">
    
    <link href="./css/base-admin-3.css" rel="stylesheet">
    <link href="./css/base-admin-3-responsive.css" rel="stylesheet">
    
    <link href="./css/dashboard.css" rel="stylesheet">   

    <link href="./css/custom.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<nav class="navbar navbar-inverse" role="navigation" >
	<div class="container-full">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
      <span class="sr-only">Toggle navigation</span>
      <i class="icon-cog"></i>
    </button>
    <a class="navbar-brand mycolor" href="index.html">$Universe Monitoring Console</a>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav navbar-right">
      <li>
		<a href="configuration.jsp">
			<i class="icon-cog"></i>
			Settings
		</a>			
	</li>
    </ul>
    
  </div><!-- /.navbar-collapse -->
</div> <!-- /.container -->
</nav>
    



    
<div class="main">

    <div class="container-full" style="margin-left:30px;margin-right:15px;">

      <div class="row">
      	
      	<div class="col-md-6 col-xs-12">
      		
      		<div class="widget stacked widget-table">
					
				<div class="widget-header">
					<i class="icon-cog"></i>
					<h3>Overview</h3>
				</div> <!-- /widget-header -->
				
				<div class="widget-content">
					
					<div class="stats">	
						
						<div class="stat">																
							UVMS server: <%=server%>:<%=port%>
							<br>
							User: <%=user%>
							<br>
							Company: <%=company%>
							<br>
							Area: <%=area%>
							<br>							
							Connected nodes: <%=nodes%>
						</div> <!-- /substat -->
						
						<div class="stat stat-time">
							Current time<br>
							<span id="clock" class="stat-currenttime"> </span>
							<br><br>Reference time window											
							<span class="stat-value"><%=window%></span>
													
						</div> <!-- /stat -->						
					</div>	
					
					
				</div> <!-- /widget-content -->
					
			</div> <!-- /widget -->	
			
			
					
			<div class="widget stacked widget-table action-table">
					
				<div class="widget-header">
					<i class="icon-th-list"></i>
					<h3>Expected Sessions</h3>
				</div> <!-- /widget-header -->
				
				<div class="widget-content">
					
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Node</th>
								<th>Session</th>
								<th>Header</th>
								<th>Expected</th>
								<th>Finish</th>
							</tr>
						</thead>
							<tbody id="expectedData">
								<tr>
									<td colspan="5" align="center">Loading data<br><img src="img/throbber.gif"/></td>
								</tr>
							</tbody>
						</table>
					
				</div> <!-- /widget-content -->
			
			</div> <!-- /widget -->
								
			
		  </div> <!-- /span6 -->
      	
      	
      	<div class="col-md-6">	
					
		
      		
      		<div class="widget stacked widget-table">
					
				<div class="widget-header">
					<i class="icon-star"></i>
					<h3>Job Status Overview</h3>
				</div> <!-- /widget-header -->
				
				<div class="widget-content">
					
					<div class="stats">		
						<div class="stat">							
							In time Jobs
							<span class="stat-value" id="intimeTasks">0</span>									
						</div> 
						
						<div class="stat">							
							Late Jobs
							<span class="stat-value" id="lateTasks">0</span>									
						</div>
					
						<div class="stat">							
							Missing Jobs
							<span class="stat-value" id="missingTasks">0</span>									
						</div>
						
						<div class="stat">							
							Unexpected Jobs
							<span class="stat-value" id="unexpectedTasks">0</span>									
						</div>
						
						<div class="stat">							
							Expected Jobs
							<span class="stat-value" id="expectedTasks">0</span>									
						</div>
						
						<div class="stat">
							Aborted Jobs
							<span class="stat-value" id="abortedUprocs">0</span>									
						</div> 				
						
						<div class="stat">							
							Running Jobs
							<span class="stat-value" id="runningUprocs">0</span>									
						</div> 
						
					</div> 
										
					<div id="chart-stats" class="stats">
						<div class="stat stat-chart">							
							<div id="donut-chart" class="chart-holder"></div> <!-- #donut -->							
						</div> <!-- /substat -->
						
					</div> <!-- /substats -->
					
				</div> <!-- /widget-content -->
					
			</div> <!-- /widget -->	
			
			
					
			<div class="widget stacked widget-table action-table">
					
				<div class="widget-header">
					<i class="icon-th-list"></i>
					<h3>Running Jobs</h3>
                    <button id="showall_button" type="button" class="btn btn-sm btn-primary pull-right" data-toggle="button">Show all</button>                
				</div> <!-- /widget-header -->
				
				<div class="widget-content">
					
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Node</th>
								<th>Session</th>
								<th>Job</th>
								<th>Start</th>
								<th>Duration</th>
							</tr>
						</thead>
							<tbody id="runningData">
								<tr>
									<td colspan="5" align="center">Loading data<br><img src="img/throbber.gif"/></td>
								</tr>
							</tbody>
						</table>
					
				</div> <!-- /widget-content -->
			
			</div> <!-- /widget -->
								
	      </div> <!-- /span6 -->
      	
      </div> <!-- /row -->

    </div> <!-- /container -->
    
</div> <!-- /main -->
    
    
<div class="footer navbar-fixed-bottom">
		
	<div class="container-full" style="margin-left:30px;margin-right:15px;">
		
		<div class="row">
			
			<div id="footer-copyright" class="col-md-6">
				&copy; 2014 Orsyp 
			</div> <!-- /span6 -->
			
			<div id="footer-terms" class="col-md-6">
				<a href="http://www.orsyp.com" target="_blank"><img src="img/orsyp.png" style="margin-right:5px;"/> Orsyp corporate site</a>
			</div> <!-- /.span6 -->
			
		</div> <!-- /row -->
		
	</div> <!-- /container -->
	
</div> <!-- /footer -->



    

<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="./js/jquery-1.9.1.min.js"></script>
<script src="./js/jquery-ui-1.10.0.custom.min.js"></script>
<script src="./js/bootstrap.min.js"></script>

<script src="./js/jquery.flot.js"></script>
<script src="./js/jquery.flot.pie.js"></script>
<script src="./js/jquery.flot.resize.js"></script>

<script src="./js/Application.js"></script>

<script src="./js/donut.js"></script>
<script src="./js/dashboard.js"></script>

<script>	
window.setInterval("refresh()",60000*<%= AppManager.getMinutesToNextTimeWindow() %>);
</script>

  </body>
</html>

