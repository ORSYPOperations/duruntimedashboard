$(function () {
	var dataArray = [0,0,0,0];
	drawDonut(dataArray);
	
});

function drawDonut (dataArray) {
	var data = [];
	
	data[0] = { label: "Timely Jobs ("+dataArray[0]+")", data:  dataArray[0]};
	data[1] = { label: "Late Jobs ("+dataArray[1]+")", data:  dataArray[1]};
	data[2] = { label: "Missing Jobs ("+dataArray[2]+")", data:  dataArray[2]};
	data[3] = { label: "Unexpected Jobs ("+dataArray[3]+")", data: dataArray[3]};
	
	//without this trick, it doesn't work
	if (dataArray[0]==0)
		data[0].data =  0.001;
	if (dataArray[1]==0)
		data[1].data =  0.001;	
	if (dataArray[2]==0)
		data[2].data =  0.001;
	if (dataArray[3]==0)
		data[3].data =  0.001;	
	
	if ((dataArray[0] + dataArray[1] + dataArray[2] + dataArray[3])==0 ) 			
		data[4]= { data: 10000};
	
	$.plot($("#donut-chart"), data,
	{
		colors: ["#fc3", "#F90", "#888", "#bbb", "#fc3"],
	        series: {
	            pie: { 
	                innerRadius: 0.45,
	                show: true
	            }
	        }
	});	
}
