var currentdate = new Date();

$(document).ready(startup());

function ajaxReadRunning() {
    new readRunning(processReadRunning);
}

function processReadRunning(data) {
	document.getElementById('runningData').innerHTML = data;
	new readCounters(processCounters);
}

function readRunning(callback) {
	var act = $( "#showall_button" ).hasClass("active");
    var req = $.ajax({
        url: "GetRunningData",
        type: "POST",
        data: { all: act }
    });

    req.done(function (reply) {
        callback(reply);
    });

    req.fail(function (jqXHR, textStatus) {
    	callback("<tr><td colspan='5' align='center'>Error loading data</td></tr>");
    });
}

//----------------------
function processCounters(data) {
	var array = data.split(',');	
	var intime = parseInt(array[0]);
	var late = parseInt(array[1]);	
	var missing = parseInt(array[2]);
	var unexpected = parseInt(array[3]);
	var expected = parseInt(array[4]);
	var abortedUpr = parseInt(array[5]);
	var runningUpr = parseInt(array[6]);
	
	document.getElementById('runningUprocs').innerHTML = runningUpr;
	document.getElementById('abortedUprocs').innerHTML = abortedUpr;	
	document.getElementById('intimeTasks').innerHTML = intime;	
	document.getElementById('lateTasks').innerHTML = late;
	document.getElementById('missingTasks').innerHTML = missing;
	document.getElementById('unexpectedTasks').innerHTML = unexpected;
	document.getElementById('expectedTasks').innerHTML = expected;
	
	var dataArray = [intime,late,missing,unexpected];
	drawDonut(dataArray);
}

function readCounters(callback) {

    var req = $.ajax({
        url: "GetCounters",
        type: "POST"
    });

    req.done(function (reply) {
        callback(reply);
    });

    req.fail(function (jqXHR, textStatus) {
    	callback("<tr><td colspan='4' align='center'>Error loading data</td></tr>");
    });
}


//----------------------

function processReadExpected(data) {
	document.getElementById('expectedData').innerHTML = data;
}

function readExpected(callback) {

    var req = $.ajax({
        url: "GetExpectedData",
        type: "POST"
    });

    req.done(function (reply) {
        callback(reply);
    });

    req.fail(function (jqXHR, textStatus) {
    	callback("<tr><td colspan='4' align='center'>Error loading data</td></tr>");
    });
}

//-------------------
function setTimeStr() {
	var today = new Date();
	var h = today.getHours();
	var m = today.getMinutes();	
	if (h < 10) 
		h = "0"+h;
    if (m < 10) 
    	m = "0"+m;
	$("#clock").text(h + ":" + m);			
}


//-------------------

function refresh() {
	window.location.href='dashboard.jsp';
}

//-------------

function startup() {
	setTimeStr();
	new readExpected(processReadExpected);
	ajaxReadRunning();	
	window.setInterval("ajaxReadRunning()",10000);
	window.setInterval("setTimeStr()", 5000);			 
}
